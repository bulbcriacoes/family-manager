import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-groups-detail',
  templateUrl: './groups-detail.component.html',
  styleUrls: ['./groups-detail.component.scss']
})
export class GroupsDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    function groupCard_showMessage(){
      let taskCard = document.querySelectorAll('.card-content');
      for ( let i = 0 ; i < taskCard.length ; i++ ){
        taskCard[i].addEventListener('click',function(){
          this.parentNode.classList.toggle('active');
        });
      }
    }

    function groupCard_displayDelete(){
      let deleteCard = document.querySelectorAll('.card-delete');
      for ( let i = 0 ; i < deleteCard.length ; i++ ){
        deleteCard[i].addEventListener('click', function(){
          this.classList.toggle('active');
        })
      }
    }

    groupCard_showMessage();
    groupCard_displayDelete()
  }

}
