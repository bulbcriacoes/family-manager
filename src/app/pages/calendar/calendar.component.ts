import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor() { }

  ngOnInit() {

      function scheduleCard_displayDelete(){
      let deleteCard = document.querySelectorAll('.card-delete');
      for ( let i = 0 ; i < deleteCard.length ; i++ ){
        deleteCard[i].addEventListener('click', function(){
          this.classList.toggle('active');
        });
      }
    }
    scheduleCard_displayDelete()
  }

}
