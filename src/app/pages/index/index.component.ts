import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    function dashboard_clearContent(){
      let dashContent = document.querySelectorAll('.dashboard-content');
      for ( let i = 0 ; i < dashContent.length ; i++){
        dashContent[i].classList.remove('active');
      }
    }

    function Tab_clearTab(){
      let tab = document.querySelectorAll('.tab');
      for ( let i = 0 ; i < tab.length ; i++ ){
        tab[i].classList.remove('active');
      }
    }

    function Tab_changeTab() {
      let tab = document.querySelectorAll('.tab');
      for ( let i = 0 ; i < tab.length ; i++ ){
        tab[i].addEventListener('click', function(){
          let _target = this.dataset.tabTarget;
          Tab_clearTab();
          dashboard_clearContent();
          document.querySelector(".dashboard-content[data-dashboard-id*='" + _target + "']").classList.add('active');
          tab[i].classList.add('active');
        });
      }
    }
    Tab_changeTab();

  }

}
