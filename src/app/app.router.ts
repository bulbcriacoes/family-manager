import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { GroupsDetailComponent } from './pages/groups-detail/groups-detail.component';
import { MemberProfileComponent } from './pages/member-profile/member-profile.component';
import { CalendarComponent } from './pages/calendar/calendar.component';


export const router: Routes = [
  { path: '', component: IndexComponent },
  { path: 'groups', component: GroupsComponent },
  { path: 'groups-detail', component: GroupsDetailComponent },
  { path: 'member-profile', component: MemberProfileComponent },
  { path: 'calendar', component: CalendarComponent },
  
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
