import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { routes } from './app.router';

import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { HeaderComponent } from './elements/header/header.component';
import { MenuComponent } from './elements/menu/menu.component';
import { DashboardCardsComponent } from './elements/dashboard-cards/dashboard-cards.component';
import { GroupsComponent } from './pages/groups/groups.component';
import { GroupsDetailComponent } from './pages/groups-detail/groups-detail.component';
import { GroupMembersComponent } from './elements/group-members/group-members.component';
import { GroupActivityStatisticsComponent } from './elements/group-activity-statistics/group-activity-statistics.component';
import { GroupBillComponent } from './elements/group-bill/group-bill.component';
import { GroupTasksComponent } from './elements/group-tasks/group-tasks.component';
import { GroupEventsComponent } from './elements/group-events/group-events.component';
import { GroupOptionsComponent } from './elements/group-options/group-options.component';
import { MemberProfileComponent } from './pages/member-profile/member-profile.component';
import { MemberLatestActivityComponent } from './elements/member-latest-activity/member-latest-activity.component';
import { ProfileOverviewComponent } from './elements/profile-overview/profile-overview.component';
import { SharedGroupsComponent } from './elements/shared-groups/shared-groups.component';
import { CalendarComponent } from './pages/calendar/calendar.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    MenuComponent,
    DashboardCardsComponent,
    GroupsComponent,
    GroupsDetailComponent,
    GroupMembersComponent,
    GroupActivityStatisticsComponent,
    GroupBillComponent,
    GroupTasksComponent,
    GroupEventsComponent,
    GroupOptionsComponent,
    MemberProfileComponent,
    MemberLatestActivityComponent,
    ProfileOverviewComponent,
    SharedGroupsComponent,
    CalendarComponent,
  ],
  imports: [
    BrowserModule,
    routes,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
