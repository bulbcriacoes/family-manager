import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupActivityStatisticsComponent } from './group-activity-statistics.component';

describe('GroupActivityStatisticsComponent', () => {
  let component: GroupActivityStatisticsComponent;
  let fixture: ComponentFixture<GroupActivityStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupActivityStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupActivityStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
