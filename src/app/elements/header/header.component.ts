import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    function menu_Open(){
      let toggleMenu = document.querySelector('.menu-icon');
      let asideMenu = document.querySelector('.aside-menu');
      toggleMenu.addEventListener('click', function(){
        this.classList.toggle('active');
        asideMenu.classList.toggle('active');
        });
    }

    menu_Open();
  }

}
