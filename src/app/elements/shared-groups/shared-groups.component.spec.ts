import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedGroupsComponent } from './shared-groups.component';

describe('SharedGroupsComponent', () => {
  let component: SharedGroupsComponent;
  let fixture: ComponentFixture<SharedGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
