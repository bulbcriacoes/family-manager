import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberLatestActivityComponent } from './member-latest-activity.component';

describe('MemberLatestActivityComponent', () => {
  let component: MemberLatestActivityComponent;
  let fixture: ComponentFixture<MemberLatestActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberLatestActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberLatestActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
