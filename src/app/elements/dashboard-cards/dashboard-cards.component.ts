import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './dashboard-cards.component.html',
  styleUrls: ['./dashboard-cards.component.scss']
})
export class DashboardCardsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    function animateInfo(){
      let cardInfo = document.querySelectorAll('.card-info');
      for ( let i = 0 ; i < cardInfo.length ; i++){
        cardInfo[i].classList.toggle('active');
      }
    }

    function dashboardCard_displayDelete(){
      let deleteCard = document.querySelectorAll('.card-delete');
      for ( let i = 0 ; i < deleteCard.length ; i++){
        deleteCard[i].addEventListener('click', function(){
          this.parentNode.classList.toggle('active');
          console.log(this.childNode);
        });
      }
    }

    dashboardCard_displayDelete();
  }

}
